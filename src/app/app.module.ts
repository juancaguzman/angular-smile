import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { A11yModule } from "@angular/cdk/a11y";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./views/home/home.component";
import { UserComponent } from "./views/user/user.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ModalComponent } from "./views/modal/modal.component";
import { MatDialogModule } from "@angular/material/dialog";
import { DemoMaterialModule } from "./material-module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { MAT_DIALOG_DATA } from "@angular/material";
import { ConfirmComponent } from "./views/confirm/confirm.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    ModalComponent,
    ConfirmComponent
  ],
  imports: [
    A11yModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    DemoMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [ModalComponent, ConfirmComponent],
  providers: [{ provide: MAT_DIALOG_DATA, useValue: [] }],
  bootstrap: [AppComponent]
})
export class AppModule {}
