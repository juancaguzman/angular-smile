import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class GeneralService {
  public url = "http://localhost:3000/";
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(`${this.url}user`);
  }

  getPositions() {
    return this.http.get(`${this.url}position`);
  }
  newUser(data) {
    return this.http.post(`${this.url}user`, data);
  }
  editUser(data) {
    return this.http.put(`${this.url}user/${data._id}`, data);
  }
  deleteUser(data) {
    return this.http.delete(`${this.url}user`, data);
  }
}
