import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { GeneralService } from "../../services/general.service";

@Component({
  selector: "app-confirm",
  templateUrl: "./confirm.component.html",
  styleUrls: ["./confirm.component.scss"],
  providers: [GeneralService]
})
export class ConfirmComponent implements OnInit {
  id;
  constructor(
    private dialog: MatDialogRef<ConfirmComponent>,
    private service: GeneralService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.id = this.data.id;
  }

  onNoClick(): void {
    this.dialog.close();
  }

  delete() {
    console.log(this.id);
    this.service.deleteUser({ id: this.id }).subscribe(
      res => console.log(res),
      err => console.log(err)
    );
  }
}
