import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ModalComponent } from "../modal/modal.component";
import { ConfirmComponent } from "../confirm/confirm.component";

import { GeneralService } from "src/app/services/general.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  providers: [GeneralService]
})
export class HomeComponent implements OnInit {
  public users;
  public colums;
  constructor(
    private dialog: MatDialog,
    private service: GeneralService,
    private router: Router
  ) {
    // this.users = [
    //   { algo: 1, name: "Hydrogen", weight: 1.0079, symbol: "H" },
    //   { algo: 2, name: "Helium", weight: 4.0026, symbol: "He" },
    //   { algo: 3, name: "Lithium", weight: 6.941, symbol: "Li" },
    //   { algo: 4, name: "Beryllium", weight: 9.0122, symbol: "Be" },
    //   { algo: 5, name: "Boron", weight: 10.811, symbol: "B" },
    //   { algo: 6, name: "Carbon", weight: 12.0107, symbol: "C" },
    //   { algo: 7, name: "Nitrogen", weight: 14.0067, symbol: "N" },
    //   { algo: 8, name: "Oxygen", weight: 15.9994, symbol: "O" },
    //   { algo: 9, name: "Fluorine", weight: 18.9984, symbol: "F" },
    //   { algo: 10, name: "Neon", weight: 20.1797, symbol: "Ne" }
    // ];
    this.colums = ["nombre", "salario", "cargo", "email", "acciones"];
  }

  ngOnInit() {
    this.getUsers();
  }
  getUsers() {
    this.service.getUsers().subscribe(
      res => {
        this.users = res;
        console.log(this.users);
      },
      err => console.log(err)
    );
  }

  editUser(el) {
    console.log("element", el);
    // this.open(el);
    localStorage.setItem("userForEdit", JSON.stringify(el));
    this.router.navigate(["user", el._id]);
  }
  deleteUser(el) {
    const modal = this.dialog.open(ConfirmComponent, {
      id: el._id
    });

    modal.afterClosed().subscribe(result => {
      this.getUsers();
    });
  }
  open() {
    const modal = this.dialog.open(ModalComponent, {
      data: { flag: true }
    });

    modal.afterClosed().subscribe(result => {
      this.getUsers();
      console.log("resultado home", result);
    });
  }
}
