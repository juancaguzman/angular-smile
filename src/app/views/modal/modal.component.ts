import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { GeneralService } from "../../services/general.service";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"],
  providers: [GeneralService]
})
export class ModalComponent implements OnInit {
  public position: any;
  public flag = true;
  public user = {
    firstName: "",
    lastName: "",
    email: "",
    position: {
      name: "",
      _id: null
    },
    salary: null
  };
  constructor(
    private dialog: MatDialogRef<ModalComponent>,
    private service: GeneralService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.getPos();
    this.user = { ...this.data.data };
    this.flag = this.data.flag;
    console.log("usuer mdal", this.user);
  }

  getPos() {
    this.service.getPositions().subscribe(
      res => {
        this.position = res;
        console.log(this.position);
      },
      err => console.log(err)
    );
  }

  registerUser() {
    this.service.newUser(this.user).subscribe(
      res => console.log(res),
      err => console.log(err)
    );
  }

  editUser() {
    console.log(this.user);
    this.service.editUser(this.user).subscribe(
      res => {
        this.data = this.user;

        console.log("red edit", res);
      },
      err => console.log(err)
    );
  }
  onNoClick(): void {
    this.dialog.close();
  }
}
