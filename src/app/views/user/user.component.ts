import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ModalComponent } from "../modal/modal.component";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
  public user;
  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("userForEdit"));
  }

  open() {
    const modal = this.dialog.open(ModalComponent, {
      data: {
        data: {
          _id: this.user._id,
          firstName: this.user.firstName,
          lastName: this.user.lastName,
          email: this.user.email,
          position: this.user.position._id,
          salary: this.user.salary
        },
        flag: false
      }
    });

    modal.afterClosed().subscribe(result => {
      console.log("result fater", result);
    });
  }
}
